# -*- coding: utf-8 -*-

from grab import Grab
from grab.selector import Selector
from lxml.html import fromstring
from grab.tools import text
from grabmodels.model_file import ModelTableFile
import re
import glob

class SpiderFiles(object):
    def __init__(self):
        self.__filename = glob.glob(u'ankets/*.html')
        self.__xpath_title = u'//title'
        self.__xpath_age = u'.//*[@id="self-age"]'
        self.__pattern_age = re.compile(r"\s\D{1,}")
        self.__query_db = ModelTableFile()

    def spiderData(self):
        for fl in self.__filename:
            temp = fl.split('/')
            data = open(fl).read()
            g = Grab(data)
            filename = temp[1]
            title = g.doc.select(self.__xpath_title).text()
            age = g.doc.select(self.__xpath_age).text()

            if type(age) is str:
                age = 0
            else:
                age_re = re.sub(self.__pattern_age, '', age)
                age = int(age_re.decode('cp866').encode('UTF-8'))
            self.__query_db.write(filename, title, age)

    def showData(self):
        rows = self.__query_db.show()
        for row in rows:
            print '*'*70
            print u'Порядковый номер: ' + str(row["id"])
            print u'Имя файла: ' + row["filename"]
            print u'Название анкеты: ' + row["name"]
            print u'Возраст: ' + str(row["age"])
            print ' '

    def closeDb(self):
        self.__query_db.close()

if __name__ == '__main__':
    sp = SpiderFiles()
    sp.spiderData()
    sp.showData()
    sp.closeDb()

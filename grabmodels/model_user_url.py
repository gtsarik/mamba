# -*- coding: utf-8 -*-

import sqlite3 as lite
import sys

class ModelTableUserUrl(object):
    def __init__(self):
        self.__table = 'user_url'
        self.__conn = self.connect()

    def connect(self):
        try:
            return lite.connect('base.db')
        except lite.Error, e:
            print('ERROR connection base')
            sys.exit(1)

    def write(self, url):
        query = '''
            INSERT INTO %s (url)
                VALUES ('%s');
            ''' % (self.__table, url)
        cursor = self.__conn.cursor()
        cursor.execute(query)
        self.__conn.commit()

    def getData(self):
        with self.__conn:
            self.__conn.row_factory = lite.Row
            query = '''
                SELECT * FROM %s;
                ''' % (self.__table)
            cursor = self.__conn.cursor()
            cursor.execute(query)
            rows = cursor.fetchall()
            self.__conn.commit()

        return rows

    def close(self):
        self.__conn.close()

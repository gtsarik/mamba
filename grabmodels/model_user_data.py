# -*- coding: utf-8 -*-

import sqlite3 as lite
import sys

class ModelTableUserData(object):
    def __init__(self):
        self.__table = 'user_profile'
        self.__conn = self.connect()

    def connect(self):
        try:
            return lite.connect('base.db')
        except lite.Error, e:
            print('ERROR connection base')
            sys.exit(1)

    def write(self, id_user, url, name, age, location):
        query = '''
            INSERT INTO %s (id_user, url, name, age, location)
                VALUES ('%s', '%s', '%s', %d, '%s');
            ''' % (self.__table, id_user, url, name, age, location)
        cursor = self.__conn.cursor()
        cursor.execute(query)
        self.__conn.commit()

    def show(self):
        with self.__conn:
            self.__conn.row_factory = lite.Row
            query = '''
                SELECT * FROM %s;
                ''' % (self.__table)
            cursor = self.__conn.cursor()
            cursor.execute(query)
            rows = cursor.fetchall()
            self.__conn.commit()

        return rows

    def delete(self):
        query = '''
            DELETE FROM %s;
            ''' % (self.__table)
        cursor = self.__conn.cursor()
        cursor.execute(query)
        self.__conn.commit()

    def close(self):
        self.__conn.close()

# -*- coding: utf-8 -*-

from lxml.html import fromstring
import urllib
import lxml.html
import re
import urllib2
from grabmodels.model_user_data import ModelTableUserData
# from grabmodels.model_user_url import ModelTableUserUrl

class SpiderUserData(object):
    def __init__(self):
        self.__main_url = u'http://www.mamba.ru/ru/search.phtml?ia=M&lf=F&af=22&at=35&wp=1&wv=0&wvc=0&ni=1&wr=0&t=a&sz=b&s_c=0_0_0_0&geo=0&s_tg=&'
        self.__xpath_title = u'.//title/text()'
        self.__xpath_age = u'.//span[@id="self-age"]'
        self.__xpath_location = u'.//*[@id="Anketa_Info"]/div[1]/div[2]/div[1]/div/text()'
        self.__pattern_title = re.compile(r"\s\d{1,}")
        self.__pattern_age = re.compile(r"\s\D{1,}")
        self.__pattern_location = re.compile(r"\s\d{1,}")
        self.__pattern_url = re.compile(r'<div class="btn-group-item selected"><a class="inset " href="(.+?)">')
        self.__pattern_user = re.compile(r'a class="u-name" href="(.+?)"')
        self.__pattern_user2 = re.compile(r'a class="u-name MustBeReal" href="(.+?)"')
        self.__offset = 0
        self.__spider = True
        self.__query_user_tab = ModelTableUserData()

    def spiderUrls(self, number):
        count = 1
        while count <= number and self.__spider:
            prefix_url = 'offset=' + str(self.__offset)
            full_url = self.__main_url + prefix_url
            html = urllib2.urlopen(full_url).read()
            btn_selected = self.__pattern_url.findall(html)
            list_btn = btn_selected[0].split('&')
            prefix_btn = list_btn[-1]

            if prefix_url == prefix_btn:
                user_selected = self.__pattern_user.findall(html)
                user_selected2 = self.__pattern_user2.findall(html)
                all_user = user_selected + user_selected2
                for x in xrange(len(all_user)):
                    id_user = all_user[x].split('/')
                    id_user = id_user[4].split('?')
                    if count <= number:
                        self.spiderData(all_user[x], id_user[0])
                        count += 1
                    else:
                        break
                self.__offset += 10
            else:
                self.__spider = False

    def spiderData(self, url, id_user):
        page = urllib.urlopen(url).read().decode('utf-8')
        doc = lxml.html.document_fromstring(page)
        name_xp = (doc.xpath(self.__xpath_title))[0]

        if name_xp == u'Анкета закрыта':
            location_xp = 'None'
            age_xp = 0
        else:
            if type(age_xp) is str:
                age_xp = 0
            else:
                try:
                    age_xp = (doc.xpath(self.__xpath_age))[0].text
                    age_re = re.sub(self.__pattern_age, '', age_xp)
                    age_xp = int(age_re.decode('cp866').encode('UTF-8'))
                except IndexError:
                    age_xp = 336699
            try:
                location_xp = (doc.xpath(self.__xpath_location))[1].strip()
                location_xp = location_xp.split('.')
                location_xp = location_xp[1].strip().replace(", ", "/")
            except IndexError:
                new_location = name_xp.split(',')
                location_xp = new_location[2] + ' /// ' + new_location[3]

        self.__query_user_tab.write(id_user, url, name_xp, age_xp, location_xp)

    def showData(self):
        rows = self.__query_user_tab.show()
        for row in rows:
            print '*'*70
            print u'Порядковый номер: ' + str(row["id"])
            print u'ID пользователя: ' + row["id_user"]
            print u'URL анкеты: ' + row["url"]
            print u'Название анкеты: ' + row["name"]
            print u'Возраст: ' + str(row["age"])
            print u'Страна/Город: ' + row["location"]
            print ' '

    def deleteFromData(self):
        self.__query_user_tab.delete()

    def closeDb(self):
        self.__query_user_tab.close()

if __name__ == '__main__':
    sp = SpiderUserData()
    # sp.spiderUrls(1000)
    sp.showData()
    sp.closeDb()
    # sp.deleteFromData()
